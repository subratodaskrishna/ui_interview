import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { Todo } from './todo.model';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Component({
    templateUrl: 'todo.component.html',
    styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
    searchText$ = new Subject<string>();
    todoList: Todo[] = [];
    completedList: Todo[] = [];
    nonCompletedList: Todo[] = [];
    loading = false;
    @ViewChild("eDialog") eDialog!: TemplateRef<any>;
    @ViewChild("aDialog") aDialog!: TemplateRef<any>;
    constructor(
        private _todoService: TodoService,
        private mdialog: MatDialog
    ) { }

    ngOnInit() {
        this.loading = true;
        this.getAllTodo();
        this.searchText$
            .pipe(
                debounceTime(500),
                distinctUntilChanged()
            ).subscribe((text: string) => {
                if (text && text.length) {
                    this.nonCompletedList = this.todoList.filter(x => !x.completed && x.title?.startsWith(text));
                } else {
                    this.nonCompletedList = this.todoList.filter(x => !x.completed);
                }
            });
    }

    onSearchTextChange(text: any) {
        this.searchText$.next(text.target.value);
    }

    getAllTodo() {
        this._todoService.getTodoList().subscribe(result => {
            this.loading = false;
            this.todoList = result;
            this.nonCompletedList = result.filter(x => !x.completed);
            this.completedList = result.filter(x => x.completed);
        }, error => {
            this.loading = false;
            alert(error);
        });
    }

    onAdd(add: Todo) {
        this.loading = true;
        this._todoService.addTodo(add).subscribe(() => {
            this.loading = false;
            alert('Added Successfully ' + add.title);
        }, error => {
            this.loading = false;
            alert(error);
        });
    }

    onDelete(id: number | undefined) {
        this.loading = true;
        this._todoService.deleteTodo(id).subscribe(() => {
            this.loading = false;
            alert('Deleted Successfully');
        }, error => {
            this.loading = false;
            alert(error);
        });
    }

    onEdit(item: Todo) {
        this.loading = true;
        this._todoService.patchTodo(item).subscribe(() => {
            this.loading = false;
            alert('Updated Successfully');
            this.mdialog.closeAll();
        }, error => {
            this.loading = false;
            alert(error);
        });
    }

    editDialog(item: Todo) {
        this.mdialog.open(this.eDialog,
          {
            data: {
              title: 'Update name or status',
              todo: item
            },
            width: '600px'
          }
        );
    }

    addDialog() {
        const item = new Todo();
        this.mdialog.open(this.aDialog,
          {
            data: {
              title: 'Add Todo',
              todo: item
            },
            width: '600px'
          }
        );
    }
}