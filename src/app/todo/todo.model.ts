export class Todo {
    id: number | undefined;
    completed: boolean | undefined;
    title: string | undefined;
    userId: number | undefined;

    constructor() {
        this.completed = false;
        this.title = '';
        this.userId = 1;
    }
}