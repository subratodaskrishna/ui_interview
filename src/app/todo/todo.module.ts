import { NgModule } from '@angular/core';

import { TodoComponent } from './todo.component';
import { TodoService } from '../services/todo.service';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    imports: [
        FormsModule,
        BrowserModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule
    ],
    exports: [],
    declarations: [TodoComponent],
    providers: [TodoService],
})
export class TodoModule { }
