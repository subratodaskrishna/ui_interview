import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../todo/todo.model';

export const SERVER_URL: string = "https://jsonplaceholder.typicode.com/todos";

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) { }

  getTodoList(): Observable<Todo[]> {
    return this.http.get<Todo[]>(SERVER_URL);    
  }

  addTodo(data: Todo): Observable<any> {
    return this.http.post<any>(SERVER_URL, data);
  }

  deleteTodo(id: number | undefined): Observable<any> {
    return this.http.delete<any>(SERVER_URL + '/' + id);
  }

  patchTodo(todo: Todo): Observable<any> {
    return this.http.patch<any>(SERVER_URL + '/' + todo.id, todo);
  }
}
